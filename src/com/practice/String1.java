package com.practice;

public class String1 {

	public static void main(String[] args) {
		{
	        String columnist1 = "King";
	        String columnist2 = "King";
	        String columnist3 = "Prince";

	        boolean equals1 = columnist1.equals(columnist2);
	        boolean equals2 = columnist1.equals(columnist3);

	        System.out.println("\"" + columnist1 + "\" equals \"" +
	            columnist2 + "\"? " + equals1);
	        System.out.println("\"" + columnist1 + "\" equals \"" +
	            columnist3 + "\"? " + equals2);
	    }
	}
	}


